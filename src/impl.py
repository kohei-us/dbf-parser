########################################################################
#
#  Copyright (c) 2010 Kohei Yoshida
#  
#  Permission is hereby granted, free of charge, to any person
#  obtaining a copy of this software and associated documentation
#  files (the "Software"), to deal in the Software without
#  restriction, including without limitation the rights to use,
#  copy, modify, merge, publish, distribute, sublicense, and/or sell
#  copies of the Software, and to permit persons to whom the
#  Software is furnished to do so, subject to the following
#  conditions:
#  
#  The above copyright notice and this permission notice shall be
#  included in all copies or substantial portions of the Software.
#  
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
#  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
#  OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
#  NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
#  HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
#  WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
#  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
#  OTHER DEALINGS IN THE SOFTWARE.
#
########################################################################

# A rough description of the structure of DBF database is found here:
# http://www.clicketyclick.dk/databases/xbase/format/dbf.html

import struct

class convert_error: pass

def str2int (bytes):

    n = len(bytes)
    if n == 0:
        raise convert_error

    if n == 1:
        # byte - 1 byte
        return struct.unpack('B', bytes)[0]
    elif n == 2:
        # short - 2 bytes
        return struct.unpack('H', bytes)[0]
    elif n == 4:
        # int, long - 4 bytes
        return struct.unpack('L', bytes)[0]


class dbf_parser(object):

    def __init__ (self, filepath):
        self.filepath = filepath
        file = open(self.filepath, 'rb')
        self.bytes = file.read()
        file.close() 
        self.pos = 0
        self.size = len(self.bytes)

    def next (self, size=1):
        self.pos += size

    def peak (self):
        return self.bytes[self.pos:self.pos+1]

    def read (self, size=1):
        b = self.bytes[self.pos:self.pos+size]
        self.next(size)
        return b

    def read_int (self, size=1):
        i = str2int(self.read(size))
        return i

    def read_bool (self, size=1):
        return self.read_int(size) != 0

    def parse (self):

        if self.size == 0:
            return

        ver   = self.read_int()
        year  = self.read_int() + 1900
        month = self.read_int()
        day   = self.read_int()

        rec_count = self.read_int(4)

        header_byte_count = self.read_int(2)
        rec_byte_count = self.read_int(2)

        print ("DBF version: %d"%ver)
        print ("last updated: %d-%d-%d"%(year, month, day))
        print ("number of records: %d"%rec_count)
        print ("number of bytes in header: %d"%header_byte_count)
        print ("number of bytes in each record: %d"%rec_byte_count)

        self.next(2) # 2-bytes reserved

        dbf4partial = self.read_bool()
        dbf4encrypt = self.read_bool()

        print ("DBF 4 partial transaction: %d"%dbf4partial)
        print ("DBF 4 encryption: %d"%dbf4encrypt)

        self.next(12) # reserved for multi-user processing
        mdx_exists = self.read_bool()
        lang_driver_id = self.read_int()

        print ("language driver ID: %d"%lang_driver_id)

        self.next(2) # reserved; filled with zeros

        while ord(self.peak()) != 0x0D:
            self.__parse_field_desc()

    def __parse_field_desc (self):
        name = self.read(11)
        print ("field name: %s"%name)
        fld_type = self.read()
        print ("field type: %s"%fld_type)
        address = self.read_int(4)
        print ("field data address: 0x%8.8X"%address)
        fld_len = self.read_int()
        print ("field length: %d"%fld_len)
        dec_count = self.read_int()
        print ("decimal count: %d"%dec_count)
        self.next(2) # reserved for multi-user dBase
        work_area_id = self.read_int()
        print ("work area ID: %d"%work_area_id)
        self.next(2) # reserved for multi-user dBase
        flag = self.read_int()
        print ("flag: %d"%flag)
        self.next(7) # reserved
        index_fld_flag = self.read_int()
        print ("index field flag: %d"%index_fld_flag)
